# Pokédex

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps/eps_2018_2/ep2/wikis/home).

# Como usar o projeto

* Abra o terminal no diretório onde está instalado o arquivo **ep2.jar** e execute o projeto com o comando:

```sh
java -jar ep2.jar
```



# Funcionalidades do projeto

Esse programa foi desenvolvido em Java, implementando a interface gráfica da Pokédex utilizando Java Swing e a **IDE Netbeans**.

As funcionalidades do projeto são:

* Listar os nomes de todos Pokémons;
* Listar informações do Pokémon escolhido;
* Pesquisar Pokémons pelo nome;
* Pesquisar Pokémons pelo tipo;
* Cadastrar um treinador;
* Selecionar um treinador e atribuir os Pokémons que este possui;
* Visualizar os Pokémons de um treinador;
* Tratamento de casos de erro.


# Exemplo de uso

##### Lista com todos os Pokemons

![Captura_de_tela_de_2018-11-07_12-04-33](/uploads/d930eaa8fce9453effb79c7a9f02578d/Captura_de_tela_de_2018-11-07_12-04-33.png)

##### Clique duas vezes no Pokémon desejado para detalhes

![Captura_de_tela_de_2018-11-07_12-04-53](/uploads/2a9145c20e264186dfae4b57c1577f55/Captura_de_tela_de_2018-11-07_12-04-53.png)

##### Faça buscas por nome e por tipos

![Captura_de_tela_de_2018-11-07_12-05-18](/uploads/5a045ea3ea8a65501a82e7334b49d2ed/Captura_de_tela_de_2018-11-07_12-05-18.png)

![Captura_de_tela_de_2018-11-07_12-05-48](/uploads/9506ced5b9daf4fa7a9d53b259fd8670/Captura_de_tela_de_2018-11-07_12-05-48.png)

##### Aperte o botão ADICIONAR TREINADOR para abrir a janela de treinadores e realizar o cadastro

![Captura_de_tela_de_2018-11-07_12-07-39](/uploads/fe2d8702f8d214efaa3a30d6c8d6e1b0/Captura_de_tela_de_2018-11-07_12-07-39.png)

##### Após o cadastro, aperte o botão VINCULAR POKÉMON para adicionar os pokemons desejados ao treinador 

![Captura_de_tela_de_2018-11-07_12-09-55](/uploads/535567caee9969d83bfa9be3f5c3437d/Captura_de_tela_de_2018-11-07_12-09-55.png)

##### Clique duas vezes no Pokémon cadastrado ao treinador para ver informações

![Captura_de_tela_de_2018-11-07_12-33-36](/uploads/4b626d37da8f7ed77ed19dcacf8053cb/Captura_de_tela_de_2018-11-07_12-33-36.png)

##### Selecione o treinador desejado para visualizar os pokemons vinculados

![Captura_de_tela_de_2018-11-07_16-09-44](/uploads/366ba183f24741f4486d816b388978e7/Captura_de_tela_de_2018-11-07_16-09-44.png)

##### Realize buscar por nomes dos treinadores

![Captura_de_tela_de_2018-11-07_12-13-18](/uploads/2c54e10d6cb9c4ff4ca3a0a567f075df/Captura_de_tela_de_2018-11-07_12-13-18.png)

## Bugs e problemas

* Na execução do programa, o Netbeans pode apresentar alguns erros, mas não interfere no projeto.

## Referências

* https://pokeapi.co/
* http://www.caelum.com.br/apostila-java-testes-xml-design-patterns/interfaces-graficas-com-swing/
* https://www.tutorialspoint.com/swing/swing_menu.htm


